const express = require("express");

const app = express();

app.get("/", (req, res) => {
  const { query } = req;
  res.json(query);
});

app.listen(4321, () => {
  console.log("Server is listening on port 4321");
});
